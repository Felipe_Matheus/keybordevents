"use strict";
document.addEventListener("keydown", (event) => {
  const keyName = event.key;
  if (keyName == "ArrowUp") {
    boxTop = boxTop - 10;
    document.getElementById("box").style.backgroundPositionY = "-96px";
    document.getElementById("box").style.height = "36px";
  } else if (keyName == "ArrowDown") {
    boxTop = boxTop + 10;
    document.getElementById("box").style.backgroundPositionY = "4px";
    document.getElementById("box").style.height = "36px";
  } else if (keyName == "ArrowLeft") {
    boxLeft = boxLeft - 10;
    document.getElementById("box").style.backgroundPositionY = "-32px";
    document.getElementById("box").style.height = "33px";
  } else if (keyName == "ArrowRight") {
    boxLeft = boxLeft + 10;
    document.getElementById("box").style.backgroundPositionY = "-64px";
    document.getElementById("box").style.height = "33px";
  }
  console.log(boxLeft);
  document.getElementById("box").style.left = boxLeft + "px";
  document.getElementById("box").style.top = boxTop + "px";
  console.log(boxTop);
  console.log("keydown event\n\n" + "key: " + keyName);
});

let boxTop = 200;
let boxLeft = 200;
